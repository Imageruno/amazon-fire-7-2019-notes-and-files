#To disable all un-needed Amazon and other pre-install packages
#list all packages
pm list packages
#list all disabled packages
pm list packages -d
#tablet will not finish loading if you removed/disable "amazon.fireos"
pm list packages|grep -i amazon|grep -v "amazon.fireos"|cut -d\: -f2|while read p;do pm disable "$p";done
pm list packages|grep -i kindle|cut -d\: -f2|while read p;do pm disable "$p";done
pm disable com.fireos.arcus.proxy 
pm disable com.android.providers.calendar
pm disable com.android.calendar
pm disable com.android.calculator2
pm disable com.android.music
pm disable com.android.email
pm disable com.android.deskclock
pm disable com.android.camera2
pm disable com.android.contacts
pm disable com.android.providers.contacts
pm disable com.android.providers.downloads
pm disable com.android.providers.downloads.ui
pm disable jp.co.omronsoft.iwnnime.mlaz
pm disable jp.co.omronsoft.iwnnime.languagepack.zhcn_az
